package view;

import java.time.LocalDate;

import controller.AlienDefenceController;
import model.User;
import model.persistance.IPersistance;
import model.persistanceDB.AccessDB;
import model.persistanceDB.UserDB;
import model.persistanceDummy.PersistanceDummy;
import view.menue.MainMenue;

public class StartAlienDefence {

	public static void main(String[] args) {
		
		IPersistance 		   alienDefenceModel      = new PersistanceDummy();
		AlienDefenceController alienDefenceController = new AlienDefenceController(alienDefenceModel);
		MainMenue              mainMenue              = new MainMenue(alienDefenceController);
		
		//Create TEST User
		AccessDB aDB = new AccessDB();
		new UserDB(aDB).createUser(new User(7, "TEST", "TEST", LocalDate.now(), "TEST", "11", "12345", "TEST", "TEST", "TEST", 1000, "TEST", 2.0));
		
		mainMenue.setVisible(true);
	}
}
