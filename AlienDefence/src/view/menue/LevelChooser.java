package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(AlienDefenceController aController, LeveldesignWindow leveldesignWindow, boolean toggle) {
		this.lvlControl = aController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;

		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		add(pnlButtons, BorderLayout.SOUTH);

		if(toggle) {
			JButton btnNewLevel = new JButton("Neues Level");
			btnNewLevel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					btnNewLevel_Clicked();
				}
			});
			pnlButtons.add(btnNewLevel);

			JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
			btnUpdateLevel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnUpdateLevel_Clicked();
				}
			});
			pnlButtons.add(btnUpdateLevel);

			JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
			btnDeleteLevel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnDeleteLevel_Clicked();
				}
			});
			pnlButtons.add(btnDeleteLevel);

		} else {
			
			//Play Button
			JButton playButton = new JButton("Spielen");
			playButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					startGame(aController);
				}
			});
			pnlButtons.add(playButton);
			
			JButton btnHighscore = new JButton("Highscore");
			btnHighscore.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int levelID = Integer.parseInt((String) tblLevels.getModel().getValueAt(tblLevels.getSelectedRow(), 0));
					
					new Highscore(aController.getAttemptController(), levelID);
				}
			});
			pnlButtons.add(btnHighscore);
		}
		
		pnlButtons.setBackground(Color.BLACK);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setForeground(new Color(124, 252, 0));
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);
		
		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setForeground(Color.BLACK);
		spnLevels.setBackground(Color.BLACK);
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);
		spnLevels.getViewport().setBackground(Color.BLACK);
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setForeground(Color.ORANGE);
		
		this.updateTableData();
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}

	private void startGame(AlienDefenceController aController) {
		User u = new User(1, "test", "pass");

		int levelID = Integer.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		Level lvl = aController.getLevelController().readLevel(levelID);

		Thread t = new Thread("GameThread") {

			@Override
			public void run() {
				GameController gController = aController.startGame(lvl, u);
				new GameGUI(gController).start();
			}
		};

		t.start();
	}
}
