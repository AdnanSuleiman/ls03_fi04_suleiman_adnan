package model.persistanceDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.CommunicationsException;
import com.sun.jmx.snmp.daemon.CommunicationException;

import model.User;
import model.persistance.IUserPersistance;

/**
 * databaseconnection for userobjects, Story usermanagement
 * @author Clara Zufall
 * TODO finish this class
 */
public class UserDB implements IUserPersistance{

	private AccessDB dbAccess;

	public UserDB(AccessDB dbAccess) {
		this.dbAccess = dbAccess;
	}
	
	public UserDB() {
		
	}

	/**
	 * read userdata by unique username
	 * 
	 * @param username
	 * @return userobject, null if user didn't exists
	 */
	public User readUser(String username) {
		String sql = "SELECT * FROM users WHERE login_name = ? ;";
		User user = null;
		try (Connection con = DriverManager.getConnection(this.dbAccess.getFullURL(), this.dbAccess.getUser(),
				this.dbAccess.getPassword()); PreparedStatement statement = con.prepareStatement(sql)) {

			statement.setString(1, username);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				user = new User(rs.getInt("P_user_id"), rs.getString("first_name"), rs.getString("sur_name"),
						rs.getDate("birthday").toLocalDate(), rs.getString("street"), rs.getString("house_number"),
						rs.getString("postal_code"), rs.getString("city"), rs.getString("login_name"),
						rs.getString("password"), rs.getInt("salary_expectations"), rs.getString("marital_status"),
						rs.getBigDecimal("final_grade").doubleValue());
			}

		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}

		return user;
	}
	
	public void createUser(User user) {
		update("INSERT INTO `users`(`P_user_id`, `first_name`, `sur_name`, `birthday`, `street`, `house_number`, `postal_code`, `city`, `login_name`, `password`, `salary_expectations`, `marital_status`, `final_grade`) VALUES (NULL,'" + user.getFirst_name() + "','" + user.getSur_name() + "','" + user.getBirthday() + "','" + user.getStreet() + "','" + user.getHouse_number() + "','" + user.getPostal_code() + "','" + user.getCity() + "','" + user.getLoginname() + "','" + user.getPassword() + "','" + user.getSalary_expectations() + "','" + user.getMarital_status() + "','" + user.getFinal_grade() + "')");
	}
	
	public void updateUser(User user) {
		update("UPDATE `users` SET `P_user_id`='" + user.getP_user_id() + "',`first_name`='" + user.getFirst_name() + "',`sur_name`='" + user.getSur_name() + "',`birthday`='" + user.getBirthday() + "',`street`='" + user.getStreet() + "',`house_number`='" + user.getHouse_number() + "',`postal_code`='" + user.getPostal_code() + "',`city`='" + user.getCity() + "',`login_name`='" + user.getLoginname() + "',`password`='" + user.getPassword() + "',`salary_expectations`='" + user.getSalary_expectations() + "',`marital_status`='" + user.getMarital_status() + "',`final_grade`='" + user.getFinal_grade() + "' WHERE `P_user_id` = '" + user.getP_user_id() + "'");
	}
	
	public void deleteUser(User user) {
		update("DELETE * FROM `users` WHERE `P_user_id` = '" + user.getP_user_id() + "'");
	}
	
	public ResultSet getResult(String query) {
		try {
			Connection con = DriverManager.getConnection(this.dbAccess.getFullURL(), this.dbAccess.getUser(),this.dbAccess.getPassword());
			
			PreparedStatement ps = con.prepareStatement(query);
			
			return ps.executeQuery();
		}catch (SQLException ex) {
			if(ex instanceof CommunicationsException) {
				return getResult(query);
			}
			
			ex.printStackTrace();
			
			return null;
		}
	}
	
	private void update(String query) {
		try {
			Connection con = DriverManager.getConnection(this.dbAccess.getFullURL(), this.dbAccess.getUser(),this.dbAccess.getPassword());
			
			PreparedStatement ps = con.prepareStatement(query);
			ps.executeUpdate();
		}catch (SQLException ex) {
			if(ex instanceof CommunicationsException) {
				update(query);
			}
			
			ex.printStackTrace();
		}
	}
}
