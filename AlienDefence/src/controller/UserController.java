package controller;

import java.sql.ResultSet;

import model.User;
import model.persistance.IUserPersistance;
import model.persistanceDB.UserDB;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}
	
	public void createUser(User user) {
		if(this.userPersistance.readUser(user.getLoginname()) != null) {
			new UserDB().createUser(user);
		}
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		if(checkPassword(username, passwort)) {
			return this.userPersistance.readUser(username);
		}
		
		return null;
	}
	
	public void changeUser(User user) {
		
	}
	
	public void deleteUser(User user) {
		
	}
	
	public boolean checkPassword(String username, String passwort) {
		return this.userPersistance.readUser(username).getPassword().equals(passwort);
	}
}
