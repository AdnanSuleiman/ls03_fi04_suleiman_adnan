package me.Adnan.GUI;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JLabel;

import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtBitteHierText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 477, 718);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDieserTextSoll = new JLabel("Dieser Text soll ver�ndert werden.");
		lblDieserTextSoll.setBounds(12, 26, 422, 16);
		lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblDieserTextSoll);
		
		JLabel lblAufgabeHintergrundfarbe = new JLabel("Aufgabe 1: Hintergrundfarbe �ndern");
		lblAufgabeHintergrundfarbe.setBounds(12, 68, 208, 16);
		contentPane.add(lblAufgabeHintergrundfarbe);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.RED);
			}
		});
		btnRot.setBounds(11, 100, 129, 25);
		contentPane.add(btnRot);
		
		JButton btnGrn = new JButton("Gr�n");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.GREEN);
			}
		});
		btnGrn.setBounds(152, 100, 139, 25);
		contentPane.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(303, 100, 142, 25);
		contentPane.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(11, 138, 129, 25);
		contentPane.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(UIManager.getColor("Panel.backround"));
			}
		});
		btnStandardfarbe.setBounds(152, 138, 139, 25);
		contentPane.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w�hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(JColorChooser.showDialog(contentPane, "W�hle Farbe", Color.WHITE));
			}
		});
		btnFarbeWhlen.setBounds(303, 138, 142, 25);
		contentPane.add(btnFarbeWhlen);
		
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabeText.setBounds(12, 176, 208, 16);
		contentPane.add(lblAufgabeText);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new java.awt.Font("Arial", 0, lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnArial.setBounds(11, 205, 129, 25);
		contentPane.add(btnArial);
		
		JButton btnComicSans = new JButton("Comic Sans MS");
		btnComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new java.awt.Font("Comic Sans MS", 0, lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnComicSans.setBounds(152, 205, 139, 25);
		contentPane.add(btnComicSans);
		
		JButton btnCurrierNew = new JButton("Courier New");
		btnCurrierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new java.awt.Font("Courier New", 0, lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnCurrierNew.setBounds(303, 205, 142, 25);
		contentPane.add(btnCurrierNew);
		
		txtBitteHierText = new JTextField();
		txtBitteHierText.setText("Hier bitte Text eingeben");
		txtBitteHierText.setBounds(12, 243, 433, 22);
		contentPane.add(txtBitteHierText);
		txtBitteHierText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText(txtBitteHierText.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(12, 276, 208, 25);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l�schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText("");
			}
		});
		btnTextImLabel.setBounds(237, 276, 208, 25);
		contentPane.add(btnTextImLabel);
		
		JLabel lblAufgabe = new JLabel("Aufgabe 3: Schritfarbe �ndern");
		lblAufgabe.setBounds(12, 314, 208, 16);
		contentPane.add(lblAufgabe);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.RED);
			}
		});
		btnRot_1.setBounds(12, 343, 128, 25);
		contentPane.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLUE);
			}
		});
		btnBlau_1.setBounds(152, 343, 139, 25);
		contentPane.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLACK);
			}
		});
		btnSchwarz.setBounds(303, 343, 142, 25);
		contentPane.add(btnSchwarz);
		
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr��e �ndern");
		lblAufgabeSchriftgre.setBounds(12, 383, 186, 16);
		contentPane.add(lblAufgabeSchriftgre);
		
		JButton button = new JButton("+");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new java.awt.Font(lblDieserTextSoll.getFont().getFontName(), 0, (lblDieserTextSoll.getFont().getSize() + 1)));
			}
		});
		button.setBounds(11, 412, 209, 25);
		contentPane.add(button);
		
		JButton button_1 = new JButton("-");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new java.awt.Font(lblDieserTextSoll.getFont().getFontName(), 0, (lblDieserTextSoll.getFont().getSize() - 1)));
			}
		});
		button_1.setBounds(237, 412, 208, 25);
		contentPane.add(button_1);
		
		JLabel lblAufgabe_1 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe_1.setBounds(12, 450, 186, 16);
		contentPane.add(lblAufgabe_1);
		
		JButton btnLinksbndig = new JButton("linksb�ndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(JLabel.LEFT);
			}
		});
		btnLinksbndig.setBounds(12, 479, 128, 25);
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(JLabel.CENTER);
			}
		});
		btnZentriert.setBounds(152, 479, 139, 25);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb�ndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(JLabel.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(303, 479, 142, 25);
		contentPane.add(btnRechtsbndig);
		
		JLabel lblAufgabe_2 = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe_2.setBounds(12, 517, 208, 16);
		contentPane.add(lblAufgabe_2);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(12, 546, 433, 112);
		contentPane.add(btnExit);
	}
}
